
import Header from "./components/Header/Header"
import './App.css'
import ProductList from "./components/Products/ProductList"

const App = () => {
  return (
    <div>
      <Header />
      <ProductList onDisplay = {false} />
    </div>
  )
}

export default App

import { useEffect, useState } from "react"
import axios from "axios"


const Categories = () => {


    const [categories, setCategories] = useState([]);
    useEffect(() => {
        axios.get('https://624b0e2171e21eebbcec0e9d.mockapi.io/api/v1/categories')
            .then((response) => {
                console.log(response);
                setCategories(response.data);
            })
    })

    return (
        <div>
            {categories.map(category => (
                <a href="">{category.name}</a>
            ))}
        </div>
    )
}

export default Categories


import HeaderLogo from './HeaderLogo'
import HeaderSearchBar from './HeaderSearchBar'
import HeaderMenu from './HeaderMenu'
import './Header.css'

const Header = () => {
    return (
        <div className='header-control'>
            <HeaderLogo />
            <HeaderSearchBar />
            <HeaderMenu />
        </div>
    )
}

export default Header



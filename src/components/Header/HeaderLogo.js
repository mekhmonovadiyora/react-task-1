import headerLogo from '../Assets/HeaderLogo.png'


const HeaderLogo = () => {
    return (
        <img src={headerLogo} />
    )
}

export default HeaderLogo

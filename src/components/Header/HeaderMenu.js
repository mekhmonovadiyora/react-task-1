
import { FaRegHeart } from 'react-icons/fa'
import { RiBarChart2Fill } from 'react-icons/ri'
import { BsCart2 } from 'react-icons/bs'
import { BsPerson } from 'react-icons/bs'
import './HeaderMenu.css'



const HeaderMenu = () => {
    return (
        <div className='header-menu__container'>
            <div className='header-menu__control'>
                <FaRegHeart className='icon' />
                <p>Избранное</p>
            </div>
            <div className='header-menu__control'>
                <RiBarChart2Fill className='icon' />
                <p>Сравнение</p>
            </div>
            <div className='header-menu__control'>
                <BsCart2 className='icon'/>
                <p>Корзина</p>
            </div>
            <div className='header-menu__control'>
                <BsPerson className='icon'/>
                <p>Войти</p>
            </div>
        </div>
    )
}

export default HeaderMenu

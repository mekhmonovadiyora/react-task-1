import { FaSearch } from 'react-icons/fa'
import './HeaderSearchBar.css'

const HeaderSearchBar = () => {
    return (
        <div>
            <input type="text" placeholder="Искать товары" />
            <button className='search_button' type="submit"><FaSearch  className='fa'/></button>
        </div>
    )
}

export default HeaderSearchBar

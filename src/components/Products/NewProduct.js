
import { FaRegHeart } from 'react-icons/fa'
import { RiBarChart2Line } from 'react-icons/ri'
import './Product.css'


const Product = (props) => {

    let newProduct = props.newProduct
    let oldProduct = props.oldProduct

    return (

        <div className='product_control'>

            <img className="product_img" src={newProduct.image} />
            <p className="product_name" >{newProduct.name}</p>
            <p className={`${newProduct.old_price ? 'old_price' : 'fff'}`}>
                {(newProduct.old_price * 10000).toLocaleString('en-US') + ' сум'}
                </p>
            <h3 className="product_price">
                {(newProduct.price * 10000).toLocaleString('en-US') + ' сум'}
                </h3>
            <p className={`${newProduct.monthly_pay ? 'product_stock' : 'fff'}`} >
                {'От ' + (newProduct.monthly_pay * 10000).toLocaleString('en-US') + ' сум/12 мес'}
                </p>
            <div className="product_actions">

                <button className={`${newProduct.in_stock == 0 ? 'btn_backet btn_empty' : 'btn_backet btn_full'}`}>В корзину</button>
                <RiBarChart2Line className='icon' />
                <FaRegHeart className='icon' />

            </div>
        </div>

    )
}

export default Product

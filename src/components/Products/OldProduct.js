
import { FaRegHeart } from 'react-icons/fa'
import { RiBarChart2Line } from 'react-icons/ri'
import './Product.css'


const Product = (props) => {

    let oldProduct = props.oldProduct

    return (

        <div className='product_control'>

            <img className="product_img" src={oldProduct.image} />
            <p className="product_name" >{oldProduct.name}</p>
            <p className={`${oldProduct.old_price ? 'old_price' : 'fff'}`}>
                {(oldProduct.old_price * 10000).toLocaleString('en-US') + ' сум'}
            </p>
            <h3 className="product_price">
                {(oldProduct.price * 10000).toLocaleString('en-US') + ' сум'}
                </h3>
            <p className={`${oldProduct.old_price ? 'product_stock' : 'fff'}`} >
                {'От ' + (oldProduct.monthly_pay * 10000).toLocaleString('en-US') + ' сум/12 мес'}
                </p>
            <div className="product_actions">

                <button className={`${oldProduct.in_stock==0 ? 'btn_backet btn_empty' : 'btn_backet btn_full'}`}>
                    В корзину
                </button>
                <RiBarChart2Line className='icon' />
                <FaRegHeart className='icon' />

            </div>
        </div>

    )
}

export default Product

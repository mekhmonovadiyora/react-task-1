import { useEffect, useState } from 'react'
import axios from 'axios'
import NewProduct from './NewProduct'
import OldProduct from './OldProduct'
import './ProductList.css'


const ProductList = () => {

    const [newProducts, setNewProducts] = useState([])
    const [oldProducts, setOldProducts] = useState([])
    const [display, setDisplay] = useState(false)

    useEffect(()=> {
        axios.get('https://624b0e2171e21eebbcec0e9d.mockapi.io/api/v1/products').then(response => {
            let newP = []
            let oldP = []

            response.data.map(product => {
                if (product.status === 'new') {
                    newP.push(product)
                } else {
                    oldP.push(product)
                }
            })

            setNewProducts(newP)
            setOldProducts(oldP)
        }) 
    }, [])
    const onDisplay =()=> {
        return !display ? setDisplay(true) : setDisplay(false)
    }

    return (
        <div>
            <div className='product_list'>
                <h1 className="products_title">Новинки</h1>
                <div className='products_control'>
                    {newProducts.map(item =>
                        <NewProduct key={item.id} title="Новинки" newProduct={item} />
                    )}
                </div>
            </div>
            <button onClick={onDisplay} className='btn_view_all'>Смотреть все</button>
            <div className={ `${!display ? 'displayNone' : 'product_list'}`}>
                <h1 className="products_title">Выбор покупателей</h1>
                <div className='products_control'>
                    {oldProducts.map(item =>
                        <OldProduct key={item.id} title="Выбор покупателей" oldProduct={item} />
                    )}
                </div>
            </div>
        </div>
    )
}

export default ProductList

